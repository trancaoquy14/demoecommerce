(function () {
    angular.module('demoECommerceApp')
        .controller('ProductUploadController', ['$scope', '$http', '$uibModalInstance', function ($scope, $http, $uibModalInstance) {
            var vm = this;
            vm.clear = clear;
            $scope.doUploadFile = function () {
                var file = $scope.uploadedFile;
                var url = "/api/uploadfile";

                var data = new FormData();
                data.append('uploadfile', file);

                var config = {
                    transformRequest: angular.identity,
                    transformResponse: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }



                $http.post(url, data, config).then(function (response) {
                    $scope.uploadResult = response.data;
                    // $uibModalInstance.close();
                }, function (response) {
                    $scope.uploadResult = response.data;
                });


            };
            function clear () {
                $uibModalInstance.dismiss('cancel');
            }
        }]);
})();

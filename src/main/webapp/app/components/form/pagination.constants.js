(function() {
    'use strict';

    angular
        .module('demoECommerceApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();

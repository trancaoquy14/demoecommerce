package com.sopen.demoecommerce.service;

import com.sopen.demoecommerce.domain.Product;
import com.sopen.demoecommerce.repository.ProductRepository;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


/**
 * Service Implementation for managing Product.
 */
@Service
@Transactional
public class ProductService {

    private final Logger log = LoggerFactory.getLogger(ProductService.class);

    private final ProductRepository productRepository;

    private final Path rootLocation = Paths.get("src/upload");

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /**
     * Save a product.
     *
     * @param product the entity to save
     * @return the persisted entity
     */
    public Product save(Product product) {
        log.debug("Request to save Product : {}", product);
        return productRepository.save(product);
    }

    /**
     * Get all the products.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Product> findAll(Pageable pageable) {
        log.debug("Request to get all Products");
        return productRepository.findAll(pageable);
    }

    /**
     * Get one product by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Product findOne(Long id) {
        log.debug("Request to get Product : {}", id);
        return productRepository.findOne(id);
    }

    /**
     * Delete the product by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Product : {}", id);
        productRepository.delete(id);
    }

    public void store(MultipartFile file) {


//        try {
//            if(Files.isDirectory(rootLocation))
//            Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()));
//
//            String SAMPLE_XLSX_FILE_PATH = "./src/upload/" + file.getOriginalFilename();
//
//
//            Workbook workbook = WorkbookFactory.create(new File(SAMPLE_XLSX_FILE_PATH));
//
//            Sheet sheet = workbook.getSheetAt(0);
//
//            System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
//            for (Row row : sheet) {
//                System.out.println(row.getCell(0).getStringCellValue());
//
//                String product_name = row.getCell(0).getStringCellValue();
//                save(new Product(product_name));
//            }
//
//            workbook.close();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//
//        } catch (InvalidFormatException e) {
//            e.printStackTrace();
//
//        }


    }


}


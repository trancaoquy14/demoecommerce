/**
 * View Models used by Spring MVC REST controllers.
 */
package com.sopen.demoecommerce.web.rest.vm;
